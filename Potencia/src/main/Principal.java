package main;

import entitie.Potencia;
import java.util.Scanner;

public class Principal {

	static Potencia p = new Potencia();
	static final int QTD = 10;
	static Scanner sc = new Scanner(System.in);

	public static void main(String[] args) {
		int n, e;

		n = numero();
		e = expoente();
		exibir(QTD, n, e);
	}

	public static int numero() {
		int n;
		System.out.println("N�mero: ");
		n = sc.nextInt();
		return n;
	}

	public static int expoente() {
		int e;
		System.out.println("Expoente: ");
		e = sc.nextInt();
		return e;
	}

	public static void exibir(int QTD, int n, int e) {
		System.out.println(p.potencia(n, e));
	}
}
