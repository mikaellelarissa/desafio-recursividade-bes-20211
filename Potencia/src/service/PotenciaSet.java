package service;

public interface PotenciaSet {
	
	public int potencia(int n, int e);
	
}
