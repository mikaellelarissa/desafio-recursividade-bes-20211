package entitie;

import service.PotenciaSet;

public class Potencia implements PotenciaSet {

	@Override
	public int potencia(int n, int e) {
		if (e == 0) {
			return 1;
		} else {
			return n * potencia(n, e - 1);
		}
	}

}
